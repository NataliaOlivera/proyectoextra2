const {Pool} = require('pg');

const pool = new Pool({
    user:'postgres',
    host:'localhost',
    password:'nati1997',
    database:'Coleccionistas' ,
    port:5432
});


const dbConnection = async() => {
    try {   
        console.log('BASE DE DATOS CONECTADA');

    } catch (error) {
        console.log(error);
        throw new Error('ERROR AL CONECTAR BASE DE DATOS')
    }
}

module.exports = {
    dbConnection,
    pool
}