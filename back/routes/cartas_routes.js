const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarCartas,
        crearCarta,
        modificarCarta,
        eliminarCarta,
        buscarCarta} = require ('../controllers/cartas_controllers');

const router = Router();

router.get('/', visualizarCartas ); //R

router.post('/', crearCarta );//C

router.put('/:id', modificarCarta );//U

router.delete('/:id', eliminarCarta );//D

router.get('/:id', buscarCarta );


module.exports= router;