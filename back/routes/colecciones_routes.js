const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarColecciones,
        crearColeccion,
        modificarColeccion,
        eliminarColeccion,
        buscarColeccion} = require ('../controllers/colecciones_controllers');

const router = Router();

router.get('/', visualizarColecciones); //R

router.post('/', crearColeccion );//C

router.put('/:id', modificarColeccion );//U

router.delete('/:id', eliminarColeccion );//D

router.get('/:id', buscarColeccion);


module.exports= router;