const {Router} = require('express');

const {visualizarRarezas,buscarRareza} = require ('../controllers/rarezas_controllers');

const router = Router();

router.get('/', visualizarRarezas ); //R
router.get('/:id', buscarRareza);



module.exports= router;