const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarUsuarios,
       crearUsuario,
       modificarUsuario,
       eliminarUsuario,
       buscarUsuario} = require ('../controllers/usuarios_controllers');

const router = Router();

router.get('/', visualizarUsuarios); //R

router.post('/',[
       check('email','Email no valido').isEmail(),
] , crearUsuario );//C

router.put('/:id', modificarUsuario );//U

router.delete('/:id', eliminarUsuario );//D

router.get('/:id', buscarUsuario );


module.exports= router;