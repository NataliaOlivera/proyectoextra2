const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarJugadores,
        crearJugador,
        modificarJugador,
        eliminarJugador,
        buscarJugador} = require ('../controllers/jugadores_controllers');

const router = Router();

router.get('/', visualizarJugadores); //R

router.post('/', crearJugador );//C

router.put('/:id', modificarJugador );//U

router.delete('/:id', eliminarJugador );//D

router.get('/:id', buscarJugador);


module.exports= router;