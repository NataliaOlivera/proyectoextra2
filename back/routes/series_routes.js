const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarSeries,
        crearSerie,
        modificarSerie,
        eliminarSerie} = require ('../controllers/series_controllers');

const router = Router();

router.get('/', visualizarSeries); //R

router.post('/', crearSerie );//C

router.put('/:id', modificarSerie );//U

router.delete('/:id', eliminarSerie );//D



module.exports= router;