const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarRolesJugadores,
        crearRolJugador,
        modificarRolJugador,
        eliminarRolJugador} = require ('../controllers/roles_jugadores_controllers');

const router = Router();

router.get('/', visualizarRolesJugadores); //R

router.post('/', crearRolJugador );//C

router.put('/:id', modificarRolJugador );//U

router.delete('/:id', eliminarRolJugador );//D



module.exports= router;