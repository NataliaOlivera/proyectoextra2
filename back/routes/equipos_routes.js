const {Router} = require('express');
const { check } = require('express-validator');

const {visualizarEquipos,
        crearEquipo,
        modificarEquipo,
        eliminarEquipo,
        buscarEquipo} = require ('../controllers/equipos_controllers');

const router = Router();

router.get('/', visualizarEquipos); //R

router.post('/', crearEquipo );//C

router.put('/:id', modificarEquipo );//U

router.delete('/:id', eliminarEquipo );//D

router.get('/:id', buscarEquipo);


module.exports= router;