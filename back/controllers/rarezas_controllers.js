const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarRarezas = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM rareza_carta');
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR RAREZAS');
    }

}
const buscarRareza = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.rareza_carta WHERE (id_rareza = $1)', [id]);'true'
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR RAREZA');
    }
}

module.exports={visualizarRarezas,buscarRareza}