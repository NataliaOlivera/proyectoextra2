const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarSeries = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM serie WHERE estado=$1', ['true']);
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR SERIES');
    }

}

const crearSerie = async(req=Request,res=Response) => {
    try {
       const{ fecha} =req.body;
       await pool.query('INSERT INTO public.serie ( fecha_salio_serie, estado)VALUES ($1, $2);', [ fecha, 'true']);

       res.status(200).json({
           message: 'SERIE CREADA',
           body:{
               Serie:{
                     fecha
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR SERIE');
   }
   
}

const modificarSerie = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ fecha } =req.body;

        await pool.query('UPDATE public.serie SET  fecha_salio_serie= $2  WHERE (numero_serie=$1) and (estado=$3)', [id_ruta, fecha, 'true'])
        res.status(200).json(`SERIE ${id_ruta} MODIFICADA`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR SERIE');
    }
    
}

const eliminarSerie = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.serie SET estado=$2  WHERE id_serie= $1 ', [id,'false']);
        res.status(200).json(`SERIE ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR SERIE');
    }
   
}


module.exports={
    visualizarSeries,
    crearSerie,
    modificarSerie,
    eliminarSerie
}