const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarRolesJugadores = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM rol_jugador WHERE estado=$1',['true']);
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR ROLES DE LOS JUGADORES');
    }

}

const crearRolJugador = async(req=Request,res=Response) => {
    try {
    //    const errors = validationResult(req);
    //    if(!errors.isEmpty()){
    //        return res.status(400).json(errors);
    //    }

       const{nombre} =req.body;
       await pool.query('INSERT INTO public.rol_jugador (nombre_rol, estado)VALUES ($1, $2);', [ nombre,'true']);

       res.status(200).json({
           message: 'ROL CREADO',
           body:{
               Rol:{
                   nombre
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR ROL');
   }
   
}

const modificarRolJugador = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ nombre } =req.body;

        await pool.query('UPDATE public.rol_jugador SET  nombre_rol= $2  WHERE (id_rol_jugador=$1) and (estado=$3) ', [id_ruta, nombre, 'true'])
        res.status(200).json(`ROL ${id_ruta} MODIFICADA`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR ROL');
    }
    
}

const eliminarRolJugador = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.rol_jugador SET estado=$2  WHERE id_rol_jugador= $1 ', [id,'false']);
        res.status(200).json(`ROL ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR ROL');
    }
   
}

module.exports={
    visualizarRolesJugadores,
    crearRolJugador,
    modificarRolJugador,
    eliminarRolJugador
}