const {Request , Response} = require('express');

const bcryptjs = require ('bcryptjs');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');


const visualizarUsuarios = async(req=Request,res= Response ) => {

    try {
        const response = await pool.query('SELECT * FROM public.usuario WHERE estado = $1', ['true'])
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR USUARIOS');
    }
    

}

const crearUsuario = async(req=Request,res=Response) => {
     try {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json(errors);
        }

        const{ nombre, contrasenia, email, id_rol } =req.body;
        const salt = bcryptjs.genSaltSync();
        // const existeEmail= await pool.findOne({email_usuario:email});
        // if(existeEmail){
        //     return res.status(400).json({
        //         msg: 'CORREO EXISTENTE'
        //     });
        // }
        await pool.query('INSERT INTO public.usuario( nombre_usuario, contrasenia_usuario, email_usuario, id_rol_usuario, estado) VALUES ($1, $2, $3, $4, $5 )', [ nombre, bcryptjs.hashSync(contrasenia, salt), email, id_rol, 'true' ]);

        
        
        res.status(200).json({
            message: 'USUARIO CREADO',
            body:{
                usuario:{
                     nombre, email, id_rol
                }
            }
        })
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL CREAR USUARIO');
    }

}

const modificarUsuario = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ nombre, contrasenia, email, id_rol } =req.body;

        await pool.query('UPDATE public.usuario SET nombre_usuario= $2, contrasenia_usuario= $3, email_usuario= $4, id_rol_usuario=$5  WHERE (id_usuario= $1) and (estado=$6) ', [id_ruta, nombre, contrasenia, email, id_rol, 'true' ])
        res.status(200).json(`USUARIO ${id_ruta} MODIFICADO`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR USUARIO');
    }
    
}

const eliminarUsuario = async(req=Request,res=Response) => {

     try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.usuario SET estado=$2  WHERE id_usuario= $1 ', [id,'false']);
        res.status(200).json(`USUARIO ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR USUARIO');
    }
    
}

const buscarUsuario = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.usuario WHERE (id_usuario = $1) and (estado=$2) ', [id,'true']);
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR USUARIO');
    }
}


module.exports = {
    visualizarUsuarios,
    crearUsuario,
    modificarUsuario,
    eliminarUsuario,
    buscarUsuario
}