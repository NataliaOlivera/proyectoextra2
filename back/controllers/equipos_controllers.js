const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarEquipos = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM equipo_jugador WHERE estado=$1', ['true']);
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR EQUIPOS');
    }

}

const crearEquipo = async(req=Request,res=Response) => {
    try {
    //    const errors = validationResult(req);
    //    if(!errors.isEmpty()){
    //        return res.status(400).json(errors);
    //    }

       const{ nombre} =req.body;
       await pool.query('INSERT INTO public.equipo_jugador ( nombre_equipo, estado)VALUES ($1, $2);', [ nombre, 'true']);

       res.status(200).json({
           message: 'EQUIPO CREADO',
           body:{
               equipo:{
                   nombre
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR EQUIPO');
   }
   
}

const modificarEquipo = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ nombre } =req.body;

        await pool.query('UPDATE public.equipo_jugador SET  nombre_equipo=$2  WHERE (id_equipo=$1) and (estado=$3)', [id_ruta,nombre, 'true'])
        res.status(200).json(`EQUIPO ${id_ruta} MODIFICADA`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR EQUIPO');
    }
    
}

 const eliminarEquipo = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.equipo_jugador SET estado=$2  WHERE id_equipo= $1 ', [id,'false']);
        res.status(200).json(`EQUIPO ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR EQUIPO');
    }
   
}

const buscarEquipo = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.equipo_jugador WHERE (id_equipo = $1) and (estado=$2)', [id, 'true']);
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR EQUIPO');
    }
}


module.exports={
    visualizarEquipos,
    crearEquipo,
    modificarEquipo,
    eliminarEquipo,
    buscarEquipo
}