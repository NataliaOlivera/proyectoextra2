const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarCartas = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query(
            'SELECT * FROM carta INNER JOIN jugador ON carta.id_jugador = jugador.id_jugador INNER JOIN rareza_carta ON carta.id_rareza = rareza_carta.id_rareza INNER JOIN equipo_jugador ON equipo_jugador.id_equipo=jugador.id_equipo INNER JOIN rol_jugador ON rol_jugador.id_rol_jugador=jugador.id_rol_jugador WHERE carta.estado=$1 and jugador.estado=$1',['true']
            );
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR CARTAS');
    }

}

const crearCarta = async(req=Request,res=Response) => {
    try {
    //    const errors = validationResult(req);
    //    if(!errors.isEmpty()){
    //        return res.status(400).json(errors);
    //    }

       const{jugador, rareza} =req.body;
       await pool.query('INSERT INTO public.carta(id_jugador, id_rareza, estado)VALUES ($1, $2, $3);', [ jugador, rareza, 'true']);

       res.status(200).json({
           message: 'CARTA CREADA',
           body:{
               carta:{
                    jugador, rareza
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR CARTA');
   }

}

const modificarCarta = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ jugador, rareza } =req.body;

        await pool.query('UPDATE public.carta SET  id_jugador=$2, id_rareza=$3  WHERE (id_carta=$1) and (estado=$4) ', [id_ruta, jugador, rareza,'true' ])
        res.status(200).json(`CARTA ${id_ruta} MODIFICADA`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR CARTA');
    }
    
}

const eliminarCarta = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.carta SET estado=$2  WHERE id_carta= $1 ', [id,'false']);
        res.status(200).json(`CARTA ${id} ELIMINADA`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR CARTA');
    }
   
}

const buscarCarta = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.carta WHERE (id_carta = $1) and (estado=$2)', [id,'true']);
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR USUARIO');
    }
}


module.exports={
    visualizarCartas,
    crearCarta,
    modificarCarta,
    eliminarCarta,
    buscarCarta
}