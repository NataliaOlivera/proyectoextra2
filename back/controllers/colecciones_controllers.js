const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarColecciones = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM coleccion WHERE estado=$1',['true']);
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR COLECCIONES');
    }

}

const crearColeccion = async(req=Request,res=Response) => {
    try {
    //    const errors = validationResult(req);
    //    if(!errors.isEmpty()){
    //        return res.status(400).json(errors);
    //    }

       const{descripcion} =req.body;
       await pool.query('INSERT INTO public.coleccion(descripcion_coleccion, estado )VALUES ($1, $2);', [descripcion, 'true']);

       res.status(200).json({
           message: 'COLECCION CREADA',
           body:{
               coleccion:{
                    descripcion
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR COLECCION');
   }

}

const modificarColeccion = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ descripcion } =req.body;

        await pool.query('UPDATE public.coleccion SET  descripcion_coleccion=$2  WHERE (id_coleccion=$1) and (estado=$3) ', [id_ruta,descripcion, 'true' ]);
        res.status(200).json(`COLECCION ${id_ruta} MODIFICADA`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR CARTA');
    }
    
}

const eliminarColeccion = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.coleccion SET estado=$2  WHERE id_coleccion= $1 ', [id,'false']);
        res.status(200).json(`COLECCION ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR COLECCION');
    }
   
}

const buscarColeccion = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.coleccion WHERE (id_coleccion = $1) and (estado=$2) ', [id, 'true']);
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR COLECCION');
    }
}


module.exports={
    visualizarColecciones,
    crearColeccion,
    modificarColeccion,
    eliminarColeccion,
    buscarColeccion
}