const {Request , Response} = require('express');

const {validationResult} = require ('express-validator');

const { json } = require('express/lib/response');

const {pool} = require('../database/config');

const visualizarJugadores = async(req=Request,res= Response ) => {
    try {
        const response = await pool.query('SELECT * FROM jugador WHERE estado=$1',['true']);
        res.status(200).json(response.rows); 
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR JUGADORES');
    }

}

const crearJugador = async(req=Request,res=Response) => {
    try {
    //    const errors = validationResult(req);
    //    if(!errors.isEmpty()){
    //        return res.status(400).json(errors);
    //    }

       const{ nombre, apellido, equipo, rol, foto} =req.body;
       await pool.query('INSERT INTO public.jugador ( nombre_jugador, apellido_jugador, id_equipo, id_rol_jugador, foto_jugador, estado)VALUES ($1, $2,$3, $4, $5, $6);', [nombre, apellido, equipo, rol, foto, 'true']);

       res.status(200).json({
           message: 'JUGADOR CREADO',
           body:{
               jugador:{
                    nombre, apellido, equipo, rol, foto
               }
           }
       })
   } catch (error) {
       console.log(error);
       res.status(500).json('ERROR AL CREAR JUGADOR');
   }
   
}

const modificarJugador = async(req=Request,res=Response) => {
    try {
        const id_ruta = parseInt(req.params.id);
        const{ nombre, apellido, equipo, rol, foto } =req.body;

        await pool.query('UPDATE public.jugador SET  nombre_jugador= $2, apellido_jugador=$3, id_equipo=$4, id_rol_jugador=$5, foto_jugador=$6  WHERE (id_jugador=$1) and (estado=$7)', [id_ruta, nombre, apellido, equipo, rol, foto, 'true'])
        res.status(200).json(`JUGADOR ${id_ruta} MODIFICADO`);

    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL MODIFICAR JUGADOR');
    }
    
}

const eliminarJugador = async(req=Request,res=Response) => {

    try {
        const id = parseInt(req.params.id);
        await pool.query('UPDATE public.jugador SET estado=$2  WHERE (id_jugador= $1) ', [id,'false']);
        res.status(200).json(`JUGADOR ${id} ELIMINADO`);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL ELIMINAR JUGADOR');
    }
   
}

const buscarJugador = async(req=Request,res=Response ) => {
    
    try {
        const id = parseInt(req.params.id);
        const response = await pool.query('SELECT * FROM public.jugador WHERE (id_jugador = $1) and (estado=$2)', [id, 'true']);
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error);
        res.status(500).json('ERROR AL VISUALIZAR JUGADOR');
    }
}


module.exports={
    visualizarJugadores,
    crearJugador,
    modificarJugador,
    eliminarJugador,
    buscarJugador
}