const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');

class Server{

    constructor(){
        this.index =express();
        this.port = process.env.port; //mi puerto
        this.usuariosPath= '/api/usuarios';
        this.cartasPath = '/api/cartas';
        this.coleccionesPath='/api/colecciones';
        this.equiposPath='/api/equipos';
        this.jugadoresPath='/api/jugadores';
        this.rolesJugadoresPath='/api/roles-jugadores';
        this.seriesPath='/api/series';
        this.rarezasPath='/api/rarezas';


        //CONEXION BD
         this.connectionDB();

        //middlewares

        this.middlewares();

        //rutas de aplicacion 
        this.routes(); //mis rutas
        
    }

    async connectionDB(){
        await dbConnection();
    }

    middlewares(){
        //CORS
        this.index.use(cors());

        //Lectura y parseo del body
        this.index.use(express.json());

        //directorio publico
        this.index.use(express.static('public'));
    }

    routes(){
        this.index.use(this.usuariosPath,require('../routes/usuarios_routes'));
        this.index.use(this.cartasPath, require('../routes/cartas_routes'));
        this.index.use(this.coleccionesPath, require('../routes/colecciones_routes'));
        this.index.use(this.equiposPath, require('../routes/equipos_routes'));
        this.index.use(this.jugadoresPath, require('../routes/jugadores_routes'));
        this.index.use(this.rolesJugadoresPath, require('../routes/roles_jugadores_routes'));
        this.index.use(this.seriesPath, require('../routes/series_routes'));
        this.index.use(this.rarezasPath, require('../routes/rarezas_routes'));
    }
    
    listen(){
        this.index.listen(this.port, () => {
            console.log('servidor corriendo en puerto', this.port);
        })
    }
}



module.exports = Server;